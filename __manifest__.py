# -*- coding: utf-8 -*-
{
    'name': "burgos",

    'summary': """
        Módulo especial para implementación de Odoo en Empresa Burgos S.R.L.""",

    'description': """
        
    """,

    'author': "Plusvalía - BigBang",
    'website': "http://plusvalia.com.ar",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product', 'stock', 'purchase', 'l10n_ar','l10n_latam_base'],

    # always loaded
    'data': [
        'security/product_security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/proveedor.xml',
        'views/familia.xml',
        'views/grupo.xml',
        'views/marca.xml',
        'views/product_template.xml',
        'views/templates.xml',
        'views/listaprecio.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
