# -*- coding: utf-8 -*-
from odoo import fields, models

class Marca(models.Model):
    _name = 'burgos.marca'

    name = fields.Char(string='Nombre', required = True)
    #description = fields.Char(string="Descripcion")
    proveedor_id = fields.Many2one('res.partner', string="Proveedor")