# -*- coding: utf-8 -*-
from odoo import fields, models

class Familia(models.Model):
    _name = 'burgos.familia'

    name = fields.Char(string='Nombre', required = True)
    description = fields.Text(string='Descripción')
    
    