# -*- coding: utf-8 -*-
from odoo import fields, models

class Proveedor(models.Model):
    _inherit = 'res.partner'
    
    is_leal = fields.Boolean(string="Leal", help="No le vende a ningun tipo de competidor dentro de la la misma zona.")
    is_rentable = fields.Boolean(string="Rentable", help="Deja un margen de contribucion mayor al 80%")
    is_aliado = fields.Boolean(string="Aliado", help="Para realizar alianzas para estrategias de Marketing Online y Offline.")
    is_socio = fields.Boolean(string="Socio", help="Preferidos con quienes hay mucha afinidad, a los cuales se les quiere dar preferencia.")
    cod_pro = fields.Char(string="Código de Proveedor")
    fantasia = fields.Char(string="Nombre Fantasía")
    type_pro = fields.Selection([('CM', 'Compra de Mercaderia'), ('CS', 'Compra de Servicio'), ('CV', 'Compra de Productos Varios')], string="Tipo de Proveedor")
    marca_id = fields.One2many('burgos.marca', 'proveedor_id', string="Marcas")