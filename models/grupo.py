# -*- coding: utf-8 -*-
from odoo import fields, models

class Grupo(models.Model):
    _name = 'burgos.grupo'

    name = fields.Char(string='Nombre', required = True)
    description = fields.Text(string='Descripción')
    
    